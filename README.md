# PCA9685 pwm frequency tester

Test the internal oscillator of an Adafruit PCA9685 board or similar

This uses the Adafruit_PWMServoDriver library. By connecting a PWM channel back to the host Arduino we can measure the pulses using the pulseIn() function.

The PCA9685 features a nominal 25MHz internal oscillator, but the actual frequency is unlikely to be accurate, and the frequeny of the output is determined by a prescaler. In the Adafuit library the prescaler is calculated using the values of "oscillator frequency" and "PWM Frequency", when you change either this does not affect the PCA9685 directly, but rather the two values are used to choose the prescaler which is then changed by the library accordingly.

This test will use the value of pwm frequency to calculate the period of one pulse, then starting at the oscillator frequency it will measure this period and attempt to adjust this nominal frequency to find the closest match for the desired pwm output frequency. It will then report back the nearest values (high and low) to the desired pwm output frequency and calculate an estimate for the "oscillator frequency" you can use in other sketches that the library. Remember the "oscillator frequency" is only used for calculation by the library

One thing I have NOT been able to test is how the voltage at VCC affects the internal oscillator of the PCA9685. For now I would recommend using this "in place" to determine values for your project before loading your own sketch, switching to a different power source could lead to invalid results (more testing required)

## Usage

To use this sketch first connect your Arduino up to your PCA9685 board as normal (Could use I2C scanner to confirm that they are comunnicating), then connect the PWM pin as defined by PWM_CHANNEL from the PCA9685 back to the Arduino pin as defined by PIN. Caution should exersized here especially if not using a 5V arduino, make sure you have a level shifter or voltage divider if nescesary, it's up to you to make sure you don't blow either up.

Now simply upload this sketch and open up the serial monitor. If all goes well the Arduino will automatically run the test and print the results back. You can the pick the closest values for your own sketch. Now you can use the oscillator frequency for initialising your own project. While you can use the PWM frequency for calculating more accurate pulse lengths I do not recommend initialising using this value, instead use the value you initally used e.g. if you calibrated for 50 Hz and the results cam back 50.02 Hz with an oscillator frequency of 25507804 your sketch would look something like this...


```
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#define OSC_FREQ    25507804        //Theoretical value for the PCA9685 internal 25MHz oscillator
#define PWM_FREQ    50              //Intended PWM output frequency

void setup() {
    pwm.begin();
    pwm.setOscillatorFrequency(OSC_FREQ);
    pwm.setPWMFreq(OSC_FREQ);
}
```
