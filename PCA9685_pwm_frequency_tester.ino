#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>


#define COUNT   100  //number of pulses to measure
#define PIN     7   //Arduino pin to use for capture
#define OSC_FREQ    25000000        //Theoretical starting value for the PCA9685 internal 25MHz oscillator
#define PWM_FREQ    50              //Intended PWM output frequency
#define PWM_CHANNEL 0   // PCA9685 pwm channel to use for measurment. Connect to PIN on Arduino, use level shifters if appropriate
#define SCOPE_OUT   15  // PCA9685 pwm channel auxilary out for oscilloscope or logic analyser

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();
unsigned int pwmFreq = PWM_FREQ;
unsigned long oscillatorFrequency = OSC_FREQ;
unsigned long target = 1 / (pwmFreq / 500000.0);

float millis_to_hz(unsigned long pulseLength, int dutyCycle = 50) {
  unsigned long pulse = (pulseLength * 100) / dutyCycle;
  //Serial.println(pulse);
  float frequency = 1 / (pulse / 1000000.0);
  return frequency;
}

unsigned long pulseToOscFreq (int prescalar, unsigned long pulseLength, int dutyCycle = 50){
  unsigned long oscFreq = (prescalar + 1.5) * millis_to_hz(pulseLength, dutyCycle) * 4096;
  return(oscFreq);
  
}


void calibrate() {
  int count = 0;
  int prevScaler = pwm.readPrescale();
  bool calibrated = false;
  long prevAvg = 0;
  unsigned long avg = 0;
  float highFreq;
  unsigned long highOscFreq;
  float lowFreq;
  unsigned long lowOscFreq;
  int prescaler;
  
  while (!calibrated)  {
    unsigned long dur;
    prescaler = pwm.readPrescale();
    avg = 0;
    Serial.print("Oscillator frequency: ");
    Serial.println(pwm.getOscillatorFrequency());
    Serial.print("Prescaler: 0x");
    Serial.println(prescaler);
    for (int i = 0; i < COUNT; i++) {
      dur = pulseIn(PIN, HIGH);
      avg += dur;
      //Serial.println(dur);
      //delay(10);
    }
    avg = avg / COUNT;
    Serial.print("Average : ");
    Serial.println(avg);
    if (avg == target) {
      calibrated = true;
    }
    else if (avg < target) {
      while (prescaler <= prevScaler) {
        oscillatorFrequency += 10000;
        pwm.setOscillatorFrequency(oscillatorFrequency);
        pwm.setPWMFreq(pwmFreq);
        prescaler = pwm.readPrescale();
      }
      if (prevAvg >= target) {
        calibrated = true;
      }
      else prevAvg = avg;
    }
    else if (avg > target) {
      while (prescaler >= prevScaler) {
        oscillatorFrequency -= 10000;
        pwm.setOscillatorFrequency(oscillatorFrequency);
        pwm.setPWMFreq(pwmFreq);
        prescaler = pwm.readPrescale();
      }
      if ((prevAvg <= target) && (count > 0)) {
        calibrated = true;
      }
      else prevAvg = avg;
    }

    prevScaler = prescaler;
    //Serial.print("Count ");
    //Serial.println(count);
    count ++;
    if (count > 10) break;
    delay(20);    //pause a moment to let any change of PWM settle
  }

  if (avg < prevAvg) {
    lowFreq = millis_to_hz(prevAvg);
    lowOscFreq = pulseToOscFreq(prevScaler, prevAvg );
    highFreq = millis_to_hz(avg);
    highOscFreq = pulseToOscFreq(prescaler, avg );
    
  }
  else if (avg > prevAvg) {
    lowFreq = millis_to_hz(avg);
    lowOscFreq = pulseToOscFreq(prescaler, avg );
    highFreq = millis_to_hz(prevAvg);
    highOscFreq = pulseToOscFreq(prevScaler, prevAvg );
  }
  Serial.println("");
  Serial.print("Target frequency ");
  Serial.print(pwmFreq);
  Serial.println(" Hz");
  Serial.println("Nearest frequecies");
  Serial.print("Low :");
  Serial.print(lowFreq);
  //Serial.print("Prescaler :");
  //Serial.print(prescaler);
  Serial.print(" Hz       Oscillator frequency: ");
  Serial.println(lowOscFreq);
  Serial.print("High :");
  Serial.print(highFreq);
  Serial.print(" Hz       Oscillator frequency: ");
  Serial.println(highOscFreq);
}

void setup() {
  // put your setup code here, to run once:
  pinMode(PIN, INPUT);
  Serial.begin(115200);
  while (!Serial);             // Wait for serial monitor

  Serial.println("PCA9685 pwm frequency tester");
  Serial.println("");
  Serial.print("Target frequency ");
  Serial.print(pwmFreq);
  Serial.println(" Hz");
  Serial.print("target pulsetime ");
  Serial.print(target);
  Serial.println(" us");
  Serial.println("");

  pwm.begin();
  pwm.setOscillatorFrequency(oscillatorFrequency);
  pwm.setPWMFreq(pwmFreq);

  delay(10);
  pwm.setPWM(PWM_CHANNEL, 0, 2048); //set for a 50% duty cycle
#ifdef SCOPE_OUT
  pwm.setPWM(15, 0, 2048); //set for a 50% duty cycle
#endif
  delay(10);

  calibrate();
  Serial.println("Done");
}


void loop() {

}
